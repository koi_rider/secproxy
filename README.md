# secproxy

Secure proxy with nginx & certbot, packaged as a self-sufficient docker container, ready to be deployed with docker-compose.

## More information

Originally forked from [staticfloat/docker-nginx-certbot](https://github.com/staticfloat/docker-nginx-certbot) - great work by @staticfloat.
I felt this was a good start, but wanted a slightly different layout of config files and an ability to "just deploy" (i.e. rely on docker registry with tagged images instead of having to build an image every time).

## How to use

In case you want things to "just work", you need to do the following easy steps:

 1. Prepare two directories -- one for site configs (explained below) and another one for letsencrypt certs. Let's assume they are /var/secproxy/sites and /var/secproxy/letsencrypt
 1. Prepare site config file. This file name should be full domain name + .conf (e.g. yoursupersite.org.conf in case your service will have a public domain name of yoursupersite.org). Do this for all the sites you want to secure with this instance of secproxy.

        upstream your-service-name {
            server 127.0.0.1:xxxx;
        }
        server {
            listen              443 ssl;
            server_name         put-your-domain-name-here.com;
            ssl_certificate     /etc/letsencrypt/live/put-your-domain-name-here.com/fullchain.pem;
            ssl_certificate_key /etc/letsencrypt/live/put-your-domain-name-here.com/privkey.pem;

            location / {
                proxy_pass          http://your-service-name;
                proxy_set_header    Host             $host;
                proxy_set_header    X-Real-IP        $remote_addr;
                proxy_set_header    X-Forwarded-For  $proxy_add_x_forwarded_for;
                proxy_set_header    X-Client-Verify  SUCCESS;
                proxy_set_header    X-Client-DN      $ssl_client_s_dn;
                proxy_set_header    X-SSL-Subject    $ssl_client_s_dn;
                proxy_set_header    X-SSL-Issuer     $ssl_client_i_dn;
                proxy_read_timeout 1800;
                proxy_connect_timeout 1800;
            }
        }
 1. Create and start a container:

        sudo docker run -d --name secproxy --restart=unless-stopped \
                        -p 80:80 -p 443:443 \
                        -v /var/secproxy/sites:/etc/nginx/conf.d/sites \
                        -v /var/secproxy/letsencrypt:/etc/letsencrypt \
                        -e 'CERTBOT_EMAIL=your-admin-email@yoursupersite.org' \
                        registry.gitlab.com/koi_rider/secproxy:latest

Note, that in order for the above setup to work, your service needs to be available on your local network. In case you would prefer to not expose the services even to your LAN, you'll have to setup a separate network in Docker and make sure that secproxy is started on the network where your services are accessible.

## How to use in production

For production use you will likely wont more control over how you configure secproxy (and nginx). For this, you may find enclosed docker-compose file useful.
